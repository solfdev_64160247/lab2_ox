/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.patcharin.lab2.ox;

import java.util.Scanner;

/**
 *
 * @author patch
 */
public class Lab2OX {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int num;
    private static int row;
    private static int col;
  

    public static void main(String[] args) {

        printStart();
        while (true) {
            printTable();
            printTurn();
//            inputNum();
            inputRowCol();
            
            if (isWin()) {
                printTable();
                printWin();
                break;
            }
             if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();

        }
    }

    private static void printStart() {
        System.out.println("Start XO Game!!!");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print( table[i][j]+" " );
            }
            System.out.println(" ");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

//    private static void inputNum() {
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Please input in number :");
//        num = sc.nextInt();
//        table num = currentPlayer;
//    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row col :");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if(checkRow() || checkcol() || checkX1()|| checkX2()) {
            return true;
        }
        
        return false;
        
    }

    private static void printWin() {
        System.out.println(currentPlayer + "Win!!!");
    }

    private static boolean checkRow() {
        for(int i=0;i<3;i++){
            if(table[row-1][i]!=currentPlayer) return false;
        }
            
         return true;
                 
    }

    private static boolean checkcol() {
          for(int i=0;i<3;i++){
            if(table[i][col-1]!=currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkX1() {
       for (int i=0;i<3;i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
       for (int i=0;i<3;i++) {
            if (table[i][2-i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw!");
    }
}
